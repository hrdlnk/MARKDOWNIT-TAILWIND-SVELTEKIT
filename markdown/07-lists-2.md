---
title: 'Numbered list'
slug: 'lists-2'
volgorde: 7
---
A **numbered list** starts with [`1.`]{.text-blue-600}   The following items on the list starting with [`0.`]{.text-blue-600} will be [automatically]{.text-pink-600 .animate-pulse} incremented

<br>

**Recipe**
1. Get ingredients
0. Dump everything in the pan
0. Cross fingers