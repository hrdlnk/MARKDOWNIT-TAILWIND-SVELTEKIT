---
title: 'Nested list'
slug: 'lists-3'
volgorde: 8
---
Indent the **nested items** with 4 spaces

<br>

- Ingredients
    - Onions
    - Croutons
- Utensils
    - Wooden spoon
    - pan