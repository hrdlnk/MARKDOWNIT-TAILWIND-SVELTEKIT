---
title: 'Tables'
slug: 'tables'
volgorde: 4
---
| Name         | Size | Material    | Color       |
| :----------- | ---: | :---------- | :---------- |
| All Business |    9 | leather     | brown       |
| Roundabout   |   10 | hemp canvas | natural     |
| Cinderella   |   11 | glass       | transparent |

[Table 1: Shoes sizes, materials, and colors.]{.text-xs}