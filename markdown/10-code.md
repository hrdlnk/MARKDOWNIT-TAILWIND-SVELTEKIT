---
title: 'Code'
slug: 'code'
volgorde: 12
---
Three backsticks indicate **formatted code**. The language can be defined right after the first three backticks.

<br>

```html
<!DOCTYPE html />
<html lang="en">
	<head>
		<title>Page Title</title>
	</head>
	<body>
		<main>
			<section>
				<article>
					<h1>My First Heading</h1>
					<p>My first paragraph.</p>
				</article>
			</section>
		</main>
	</body>
</html>
```