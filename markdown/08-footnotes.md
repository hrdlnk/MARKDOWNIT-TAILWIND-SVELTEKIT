---
title: 'Footnotes'
slug: 'footnotes-1'
volgorde: 10
---
**Inline notes** are a quick way of adding footnotes. This^[Here’s the first footnote. It’s inlined, which isn’t great if you want your markdown to be legible] is an inline note and that’s another one.^[This text is the body of the second footnote.] Numbering is automagic :sparkles: