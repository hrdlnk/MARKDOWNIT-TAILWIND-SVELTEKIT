---
title: ''
slug: 'footnotes-2'
volgorde: 11
---
For the sake of legibility, **group footnotes** at the end of your document. Here is a short footnote reference,[^1] followed by a longer one[^longnote] and one last for the road.[^shortnote]

[^1]:
    Here is the first short footnote.

[^longnote]:
    This one is made of multiple paragraphs. Subsequent paragraphs are indented to attach them to the previous footnote.
    
    Mollit duis nostrud eiusmod anim ex ut anim incididunt laborum ut dolor velit ex. Voluptate eiusmod anim anim laboris dolore excepteur.

    Reprehenderit tempor esse id occaecat adipisicing deserunt incididunt. Ea et dolor nulla laborum nulla labore est commodo veniam. Exercitation deserunt ea veniam officia est aliquip.

    Qui laborum ad dolore et id eiusmod consectetur. Irure cupidatat adipisicing labore nisi aliquip cillum eiusmod tempor aliquip ullamco.

    Fugiat anim Lorem irure ad ut aliqua voluptate id. Ut deserunt anim laboris et laboris est quis. Elit veniam duis aute cillum dolor minim cupidatat excepteur dolor velit. Excepteur irure in in esse sit quis elit sit proident veniam ea enim sit. Irure velit mollit Lorem minim labore nulla. Incididunt voluptate ad occaecat cupidatat exercitation.

[^shortnote]:
    Ea et dolor nulla laborum nulla labore est commodo veniam. Exercitation deserunt ea veniam officia est aliquip.