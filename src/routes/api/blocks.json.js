import fs from 'fs'
import matter from 'gray-matter'

export const get = async () => {
    // const allBlockFiles = import.meta.glob('../lib/markdown/*.md')
    // const iterableBlockFiles = Object.entries(allBlockFiles)

    const iterableBlockFiles = await fs.promises.readdir('markdown')

    const allBlocks = await Promise.all(
        iterableBlockFiles.map(async (fileName) => {
            const doc = await fs.promises.readFile(`markdown/${fileName}`, 'utf8')
            // console.log('doc:', doc);
            const data = matter(doc)
            // console.log('data:', data)
            return data
        })
    )

    const sortedBlocks = allBlocks.sort((a, b) => {
        return a.volgorde - b.volgorde
    })

    return {
        body: JSON.stringify(sortedBlocks)
    }
}